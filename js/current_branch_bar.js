/**
 * Process action for status bar.
 */

(function ($) {
  Drupal.behaviors.cuurent_branch_bar = {
    attach: function (context, settings) {
      $('.branch-remove', context).click(function(e) {
        e.preventDefault();
        $('.branch-name-wrap').remove();
      });
      $('.branch-control-bar', context).click(function(e) {
        e.preventDefault();
        if($('.branch-name-wrap').hasClass('disactive')){
          $('.branch-name-wrap').removeClass('disactive');
          $(this).text('-');
        }else{
          $('.branch-name-wrap').addClass('disactive');
          $(this).text('+');
        }
      });
    }
  };
})(jQuery);
