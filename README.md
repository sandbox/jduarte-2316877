
-- SUMMARY --

The current branch bar display a current git branch of work.

-- REQUIREMENTS --

1. Run your drupal site with git version control system..

-- INSTALLATION --

1. Download and unpack the "Current Branch Bar" module directory in your
   modules folder (this will usually be "sites/all/modules/").
2. Go to "Administer" -> "Modules" and enable the "Current Branch Bar"
   module.

-- CONFIGURATION --

1. Go to "Configuration" => "Development" => "Current Branch" to configure
   the module.
2. A form is display to perform the display and position of bar.


-- CUSTOMIZATION --

If you will change the display of bar according with different configuration.


-- TROUBLESHOOTING --



-- FAQ --



-- CONTACT --

Current maintainers:
* Jeffrey Duarte Duarte (jduarte) - http://drupal.org/user/1583314

This project has been sponsored by:
* PARALLELDEVS
  Specialized in development, theming and customization of Drupal Sites.
  Visit http://www.paralleldevs.com for more information.
